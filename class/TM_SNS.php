<?php
namespace ThemeMountain {
	/**
	 * 	Loads widget
	 *
	 * @package ThemeMountain
	 * @subpackage theme-plugin
	 * @since 1.0
	 */
	class TM_SNS {
		/**
		 * variables for plugin
		*/
		public static $local_plugin_dir;
		public static $local_plugin_dir_uri;

		/**
		 * Constructor
		 */
		public function __construct( $_class_settings = array () ) {
			self::$local_plugin_dir = $_class_settings['local_plugin_dir'];
			self::$local_plugin_dir_uri = $_class_settings['local_plugin_dir_uri'];

			/**
			 * Action hook to display post_social_list
			 */
			add_action( 'tm_post_social_list', array('ThemeMountain\\TM_SNS','tm_post_social_list'));
		}

		/**
		 * Display post_social_list
		 *
		 * @since      1.1
		 */
		public static function tm_post_social_list () {
			if(!class_exists('\\ThemeMountain\\TM_Customizer')) return;
			// set arguments
			$_args = array();
			// 'use_pinterest' => '',
			if (TM_Customizer::tm_get_theme_mod('tm_post_pinterest') == true) $_args['use_pinterest'] = 'true';
			// 'use_facebook' => '',
			if (TM_Customizer::tm_get_theme_mod('tm_post_facebook') == true) $_args['use_facebook'] = 'true';
			// twitter
			if (TM_Customizer::tm_get_theme_mod('tm_post_twitter') == true) $_args['use_twitter'] = 'true';
			// googleplus
			if (TM_Customizer::tm_get_theme_mod('tm_post_googleplus') == true) $_args['use_googleplus'] = 'true';
			// linkedin
			if (TM_Customizer::tm_get_theme_mod('tm_post_linkedin') == true) $_args['use_linkedin'] = 'true';
			// email
			if (TM_Customizer::tm_get_theme_mod('tm_post_email') == true) $_args['use_email'] = 'true';
			// Page Description
			$_args['page_description'] = get_the_excerpt();
			// Image Url
			if ( has_post_thumbnail() ) {
				$_thumbnailID = get_post_thumbnail_id();
				$_args['image_url'] = wp_get_attachment_url($_thumbnailID);
			}
			// Title
			$_args['title'] = get_the_title();
			// echo shortcode
			echo tm_content_socialize($_args,'','tm_content_socialize');
		}
	}
}