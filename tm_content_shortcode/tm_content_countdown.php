<?php
use ThemeMountain\TM_Shortcodes as TM_Shortcodes;
add_shortcode( 'tm_content_countdown', 'tm_content_countdown' );
function tm_content_countdown($atts, $content, $tagname) {
	// find basename
	if(isset($this)) {
		$_base = $this->settings['base'];
	} else if (isset($tagname)) {
		$_base = $tagname;
	} else {
		$_base = FALSE;
	}

	$_class = $_output = $_css_id = $_countdown_style_class = '';
	$_class_array = array();

	extract(shortcode_atts(array(
		'el_class' => '',
		'el_id' => '',
		'countdown_style' => 'horizontal', // (dropdown)
		'countdown_date' => 'Jan 1, 2020 24:00:00', // (textfield)
		'day_unit' => 'd', // (textfield)
		'hour_unit' => 'h', // (textfield)
		'minute_unit' => 'm', // (textfield)
		'seconds_unit' => 's', // (textfield)
		'font_size' => '3.294rem', // (textfield)
		'color' => '#666', // (textfield)
		'unit_opacity' => '0.7', // (textfield)
	), $atts));

	// css ID
		$_css_id = 'tm-countdown-'.TM_Shortcodes::tm_serial_number();

	// sanitize
		$countdown_date = esc_html($countdown_date);
		$seconds_unit = esc_attr($seconds_unit);
		$minute_unit = esc_attr($minute_unit);
		$hour_unit = esc_attr($hour_unit);
		$day_unit = esc_attr($day_unit);

	// $_countdown_style_class
		if($countdown_style === 'stacked'){
			$_countdown_style_class = ' countdown-2';
		} else {
			$_countdown_style_class = ' countdown-1';
		}

	// design options
		// color
		if($color !== '') {
			TM_Shortcodes::tm_add_inline_css(".countdown.{$_css_id} .unit { color: {$color}; }");
		}
		// font size
		if($font_size !== '') {
			TM_Shortcodes::tm_add_inline_css(".countdown.{$_css_id} .unit { font-size: {$font_size}; }");
		}
		// Opacity
		if($unit_opacity !== '') {
			TM_Shortcodes::tm_add_inline_css(".countdown.{$_css_id} .unit-type { opacity: {$unit_opacity}; }");
		}

	// construct output
	$_output = "<div class='countdown{$_countdown_style_class} {$_css_id}' data-countdown data-countdown-date='{$countdown_date}' data-unit-type='d:{$day_unit};h:{$hour_unit};m:{$minute_unit};s:{$seconds_unit}'></div>";

	/** Output */
	return $_output;
}