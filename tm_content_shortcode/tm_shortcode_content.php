<?php
use ThemeMountain\TM_Shortcodes as TM_Shortcodes;
/**
 * tm_shortcode_content shortcode.
 *
 * This is for content shortcode. Not available as a VC element
 */
add_shortcode( 'tm_shortcode_content', 'tm_shortcode_content' );
function tm_shortcode_content($atts, $content, $tagname) {
	$_output ='';
	$_acquired_data = NULL;
	extract(shortcode_atts(array(
		'title' => '',
	), $atts));
	// load content of custom post type
	if(!empty($title)) {
		$_acquired_data = get_page_by_title($title,OBJECT,'tm_shortcode_content');
		if($_acquired_data && is_object($_acquired_data)) {
			$_output = $_acquired_data->post_content;
			// make sure that there will not be infinite loop
			$_output = str_replace('tm_shortcode_content', 'tm_shortcode_content_invalid', $_output);
			// enable output of tm-has-shortcode-content
			TM_Shortcodes::enable_row_with_shortcode_content(TRUE);
			$_output = TM_Shortcodes::tm_do_shortcode($_output);
			// disable output of tm-has-shortcode-content
			TM_Shortcodes::enable_row_with_shortcode_content(FALSE);
		}
	}
	return $_output;
}