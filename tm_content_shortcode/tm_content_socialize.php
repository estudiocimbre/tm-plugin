<?php
use ThemeMountain\TM_Shortcodes as TM_Shortcodes;
/**
 * Socialize "Social Share" shortcode.
 *
 * This is for content shortcode. Not available as a VC element
 */
add_shortcode( 'tm_content_socialize', 'tm_content_socialize' );
function tm_content_socialize($atts, $content, $tagname) {
	$_output = $_sns_icons = $_with_circle = $_sns_type = '';

	$_post_id = get_the_ID();

	extract(shortcode_atts(array(
		'title' => '',
		'margin_bottom' => '30',
		'margin_bottom_mobile' => '30',
		'use_pinterest' => '',
	    'use_facebook' => '',
	    'use_twitter' => '',
	    'via_username' => '',
	    'use_googleplus' => '',
	    'use_linkedin' => '',
	    'use_email' => '',
	    'image_url' => '',
	    'page_description' => '',
		'icon_size' => 'medium', // small, medium (blank), large, xlarge
	    // color settings
		'icon_color_pinterest' => '',
		'icon_color_pinterest_hover' => '',
		'icon_color_facebook' => '',
		'icon_color_facebook_hover' => '',
		'icon_color_twitter' => '',
		'icon_color_twitter_hover' => '',
		'icon_color_googleplus' => '',
		'icon_color_googleplus_hover' => '',
		'icon_color_linkedin' => '',
		'icon_color_linkedin_hover' => '',
		'icon_color_email' => '',
		'icon_color_email_hover' => '',
		// extra attributes
	    'el_id' => '', // textfield
	    'el_class' => '', // textfield
	), $atts));

	// determin title
		$_title = (empty($title)) ? get_the_title($_post_id) : $title;

	// css ID
		$_css_id = 'tm_content_socialize-'.TM_Shortcodes::tm_serial_number();

	// attributes
		$image_url = esc_url($image_url);
		$page_description = htmlspecialchars($page_description,ENT_QUOTES);
		$icon_size = esc_attr($icon_size);

	// margin
		if($margin_bottom === 'inherit') {
			$margin_bottom = '';
		} else {
			$margin_bottom = ' mb-'.esc_attr($margin_bottom);
		}
	// margin on mobile
		if($margin_bottom_mobile === 'inherit') {
			$margin_bottom_mobile = '';
		} else {
			$margin_bottom_mobile = ' mb-mobile-'.esc_attr($margin_bottom_mobile);
		}

	// construct li
		$_postID = get_the_ID();
		$_currentPostURL = get_permalink($_postID);
		if ( has_post_thumbnail() ) {
			$_thumbnailID = get_post_thumbnail_id($_postID);
			$_currentPostImage = wp_get_attachment_url($_thumbnailID);
			$_currentPostDescription = get_the_title($_postID);
		} else {
			$_currentPostImage = '';
			$_currentPostDescription = '';
		}

	// Icons
	$_social_network_sets = array('use_pinterest','use_facebook','use_twitter','use_googleplus','use_linkedin','use_email');
	// data-url prep
	$_http = is_ssl() ? 'https://' : 'http://';
	$_url = $_http . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

	foreach ($_social_network_sets as $_sns_type) {
		if($$_sns_type !== 'true') continue;
		// sns type
		$_sns_type = (!empty($_sns_type)) ? str_replace('use_','',esc_attr($_sns_type)) : '';
		// reset once
		$_data_attributes = '';
		// data url
		$_data_attributes .= " data-url='{$_url}'";
		// data-network
		$_data_attributes .= " data-network='".$_sns_type."'";
		// data-image
		if($image_url !== '') $_data_attributes .= " data-image='{$image_url}'";
		// data-description
		if(!in_array($_sns_type,array('use_facebook','use_googleplus'))) {
			$_data_attributes .= " data-description='{$page_description}'";
			// data-title
			if($_title !== '') {
				$_data_attributes .= ' data-title="'.htmlspecialchars(TM_Shortcodes::tm_wp_kses($_title),ENT_QUOTES).'"';
			}
		}
		// title
		if($_title !== '') {
			$_data_attributes .= ' title="'.htmlspecialchars(TM_Shortcodes::tm_wp_kses($_title),ENT_QUOTES).'"';
		}
		// Supported SNS
		switch($_sns_type){
			case 'email':
				$_icon_class = ' icon-email';
				$_href_attribute = 'mailto:friendsname@domain.com?&amp;subject='.esc_html__('Check out this site','thememountain-plugin').'&amp;body='.$page_description.' - '.$_url;
				// data-email
				$_data_attributes .= " data-email='yourfriend@somedomain.com'";
							// data-subject
				$_data_attributes .= " data-subject='".esc_html__('Check out this site','thememountain-plugin')."'";
				break;
			case 'facebook':
				$_icon_class = ' icon-facebook-with-circle';
				$_href_attribute = '#';
				break;
			case 'googleplus':
				$_icon_class = ' icon-google-with-circle';
				$_href_attribute = '#';
				break;
			case 'linkedin':
				$_icon_class = ' icon-linkedin-with-circle';
				$_href_attribute = '#';
				break;
			case 'pinterest':
				$_icon_class = ' icon-pinterest-with-circle';
				$_href_attribute = '#';
				break;
			case 'twitter':
				$_icon_class = ' icon-twitter-with-circle';
				$_href_attribute = '#';
				// data-user
				if($via_username !==''){
					$_data_attributes .= " data-user='".esc_attr($via_username)."'";
				}
				break;
			default:
				$_icon_class = '';
				break;
		}

		// variables
		$_icon_color = 'icon_color_'.$_sns_type;
		$_icon_color_hover = 'icon_color_'.$_sns_type.'_hover';

		// css
		if($$_icon_color !== ''){
			TM_Shortcodes::tm_add_inline_css(".{$_css_id} li a.{$_sns_type} { color:{$$_icon_color}; }");
		}
		if($$_icon_color_hover !== ''){
			TM_Shortcodes::tm_add_inline_css(".{$_css_id} li a.{$_sns_type}:hover { color:{$$_icon_color_hover}; }");
		}

		$_sns_icons .= "<li><a class='socialize {$_sns_type}{$el_class}' href='{$_href_attribute}'{$_data_attributes}'><span class='{$icon_size}{$_icon_class}'></span></a></li>";
	}

	// class / id
		$el_class = ($el_class !== '' ) ? ' '.$el_class : '';
		$el_id = TM_Shortcodes::wrap_with_id_attr($el_id);

	$_output = "<ul class='{$_css_id} social-list list-horizontal{$margin_bottom}{$margin_bottom_mobile}{$el_class}'{$el_id}>{$_sns_icons}</ul>";

	return $_output;
}