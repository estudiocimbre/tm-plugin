<?php
use ThemeMountain\TM_Shortcodes as TM_Shortcodes;
/**
 * "Social Share" shortcode.
 *
 * This is for content shortcode. Not available as a VC element
 */
add_shortcode( 'tm_content_announcement_link', 'tm_content_announcement_link' );
function tm_content_announcement_link($atts, $content, $tagname) {
	$_output = $_border_style_class = '';

	extract(shortcode_atts(array(
		'background_color' => '',
		'label_background_color' => '',
		'label_color' => '',
		'link_color' => '',
		'link_color_hover' => '',
		'label_text' => '',
		'link_text' => '',
		'border_style' => '',
	), $atts));

	// css ID
		$_css_id = 'tm_content_announcement_link-'.TM_Shortcodes::tm_serial_number();

	// sanitization
		$label_text = TM_Shortcodes::tm_wp_kses($label_text);
		$link_text = TM_Shortcodes::tm_wp_kses($link_text);

	// Border Style
		switch($border_style) {
			case 'pill':
				$_border_style_class = ' pill';
				break;
			case 'rounded':
				$_border_style_class = ' rounded';
				break;
		}


	// css
		if($background_color !==''){
			TM_Shortcodes::tm_add_inline_css(".{$_css_id} { background-color:{$background_color}; }");
		}
		if($label_background_color !==''){
			TM_Shortcodes::tm_add_inline_css(".{$_css_id} .label { background-color:{$label_background_color}; }");
		}
		if($label_color !==''){
			TM_Shortcodes::tm_add_inline_css(".{$_css_id} .label { color:{$label_color}; }");
		}
		if($link_color !==''){
			TM_Shortcodes::tm_add_inline_css(".{$_css_id} { color:{$link_color}; }");
		}
		if($link_color_hover !==''){
			TM_Shortcodes::tm_add_inline_css(".{$_css_id}:hover { color:{$link_color_hover}; }");
		}

	$_output = "<a href='#' class='{$_css_id} announcement left{$_border_style_class}'><span class='label'>{$label_text}</span><span class='message'>{$link_text}</span></a>";

	return $_output;
}