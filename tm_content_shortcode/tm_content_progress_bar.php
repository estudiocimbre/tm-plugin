<?php
use ThemeMountain\TM_Shortcodes as TM_Shortcodes;
add_shortcode( 'tm_content_progress_bar', 'tm_content_progress_bar' );
function tm_content_progress_bar($atts, $content, $tagname) {
	$_output = $label_output = $_progress_bar_content = $_progress_bar_content_mobile = '';

	extract(shortcode_atts(array(
		'margin_bottom' => '30',
		'margin_bottom_mobile' => '30',
		'progress_bar_label' => '',
		'display_as_group' => '',
		'hide_measure' => '',
		'percentage_bar' => '50',
		'measure_bar' => '%',
		'percentage_bar_2' => '30',
		'measure_bar_2' => '%',
		'percentage_bar_3' => '30',
		'measure_bar_3' => '%',
		'animate' => 'true',
		'size' => 'medium',
		'border_style' => '',
		'el_class' => '',
		'el_id' => '',
		'track_background_color' => '#eee',
		'track_border_color' => '#eee',
		'bar_background_color' => '#d0d0d0',
		'bar_border_color' => '#d0d0d0',
		'bar_background_color_2' => '#d0d0d0',
		'bar_border_color_2' => '#d0d0d0',
		'bar_background_color_3' => '#d0d0d0',
		'bar_border_color_3' => '#d0d0d0',
		'text_color' => '',
	), $atts));

	// css id
		$_css_id = 'tm-progress-bar-'.TM_Shortcodes::tm_serial_number();

	// sanitization
		$el_class = ($el_class!=='') ? ' '.esc_attr($el_class) : '';
		if(!empty($border_style)) $border_style = ' '.esc_attr($border_style);
		if(!empty($size)) $size = ' '.esc_attr($size);


	// margin
		if($margin_bottom === 'inherit') {
			$margin_bottom = '';
		} else {
			$margin_bottom = ' mb-'.esc_attr($margin_bottom);
		}
	// margin on mobile
		if($margin_bottom_mobile === 'inherit') {
			$margin_bottom_mobile = '';
		} else {
			$margin_bottom_mobile = ' mb-mobile-'.esc_attr($margin_bottom_mobile);
		}

		if($animate !== '') {
			$animate = ' horizon';
			$_data_animate_in =' data-animate-in="transX:-100%;duration:1000ms;easing:swing;"';
		} else {
			$_data_animate_in = '';
		}

	// css
		// Track background color
		$_css_spacer = ($display_as_group === 'true') ? ' ' : '';
		if ( $track_background_color !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id{$_css_spacer}.progress-bar {background-color:$track_background_color;}");
		}
		// Track border color
		if ( $track_border_color !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id{$_css_spacer}.progress-bar {border-color:$track_border_color;}");
		}

		// bar_background_color
		if ( $bar_background_color !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id .bar-1 {background-color:$bar_background_color;}");
		}
		// bar_border_color
		if ( $bar_border_color !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id .bar-1 {border-color:$bar_border_color;}");
		}

		// bar_background_color
		if ( $bar_background_color_2 !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id .bar-2 {background-color:$bar_background_color_2;}");
		}
		// bar_border_color
		if ( $bar_border_color_2 !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id .bar-2 {border-color:$bar_border_color_2;}");
		}

		// bar_background_color
		if ( $bar_background_color_3 !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id .bar-3 {background-color:$bar_background_color_3;}");
		}
		// bar_border_color
		if ( $bar_border_color_3 !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id .bar-3 {border-color:$bar_border_color_3;}");
		}

		// text_color
		if ( $text_color !== '' ) {
			TM_Shortcodes::tm_add_inline_css(".$_css_id .bar {color:$text_color;}");
		}

	// id
		$el_id = TM_Shortcodes::wrap_with_id_attr($el_id);

	// Output
	if($display_as_group == 'true'){
		// label
		if($progress_bar_label !== '') {
			$label_output = '<div><span class="progress-bar-label pull-left">'.TM_Shortcodes::tm_wp_kses($progress_bar_label).'</span></div>';
		}
		// bar contents
		if(!empty($percentage_bar)) {
			$_progress_bar_content .= "<div class='bar-1 bar{$animate}' style='width:{$percentage_bar}%;'{$_data_animate_in}>{$measure_bar}</div>";
			$_progress_bar_content_mobile .= "<div class='progress-bar{$size}'><div class='bar-1 bar{$animate}' style='width:{$percentage_bar}%;'{$_data_animate_in}>{$measure_bar}</div></div>";
		}
		if(!empty($percentage_bar_2)) {
			$_progress_bar_content .= "<div class='bar-2 bar{$animate}' style='width:{$percentage_bar_2}%;'{$_data_animate_in}>{$measure_bar_2}</div>";
			$_progress_bar_content_mobile .= "<div class='progress-bar{$size}'><div class='bar-2 bar{$animate}' style='width:{$percentage_bar_2}%;'{$_data_animate_in}>{$measure_bar_2}</div></div>";
		}
		if(!empty($percentage_bar_3)) {
			$_progress_bar_content .= "<div class='bar-3 bar{$animate}' style='width:{$percentage_bar_3}%;'{$_data_animate_in}>{$measure_bar_3}</div>";
			$_progress_bar_content_mobile .= "<div class='progress-bar{$size}'><div class='bar-3 bar{$animate}' style='width:{$percentage_bar_3}%;'{$_data_animate_in}>{$measure_bar_3}</div></div>";
		}
		// grouped bar contents
		// Progress bar group desktop
		$_output = "<div class='{$_css_id} progress-bar-group hide-on-mobile{$el_class}'>{$label_output}<div class='progress-bar {$size}{$border_style}{$margin_bottom}{$margin_bottom_mobile}'>{$_progress_bar_content}</div></div>";
		// Progress bar group mobile
		$_output .= "<div class='{$_css_id} progress-bar-group hide show-on-mobile {$border_style}{$margin_bottom}{$margin_bottom_mobile}{$el_class}'>{$label_output}{$_progress_bar_content_mobile}</div>";
	} else {
		// percentage_bar
		if($hide_measure !== 'true') {
			$_progress_bar_content = '<span class="pull-right">'.TM_Shortcodes::tm_wp_kses($percentage_bar).$measure_bar.'</span>';
		} else {
			$_progress_bar_content = '';
		}
		// label
		if($progress_bar_label !== '') {
			$label_output = '<span class="progress-bar-label">'.TM_Shortcodes::tm_wp_kses($progress_bar_label).$_progress_bar_content.'</span>';
		}
		$_output = "{$label_output}<div class='{$_css_id} progress-bar {$size}{$border_style}{$margin_bottom}{$margin_bottom_mobile}{$el_class}'{$el_id}><div class='bar-1 bar{$animate}' style='width:{$percentage_bar}%;'{$_data_animate_in}></div></div>";
	}
	/** Output */
		return $_output;
}