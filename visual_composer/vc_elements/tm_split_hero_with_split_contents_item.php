<?php
/**
	ThemeMountain tm_client_item
*/

// Checks up if the Visual Composer is activated.
	/**
		Note: please see initVisualComposer.php ... this file is included upon the vc_before_init hook.
	*/
vc_map( array(
	'name' => esc_html__( 'Split Hero With Split Content Column', 'thememountain-plugin' ),
	'base' => 'tm_split_hero_with_split_contents_item',
	// 'allowed_container_element' => false,
	'is_container' => true,
	"as_child" => array('only' => 'tm_split_hero_with_split_contents_holder'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
	'content_element' => false,
	'description' => '',
	'params' => array(
		array(
			// group Social List
			'type' => 'textfield',
			'heading' => esc_html__( 'Hero Column Name', 'thememountain-plugin' ),
			'param_name' => 'title',
			'value' => '',
			'description' => esc_html__( 'Enter a column title. Used to identify this column in the Visual Composer.', 'thememountain-plugin' ),
		),
		// Image
		ThemeMountain\TM_Vc::get_image_selector_vc_field('',esc_html__( 'Image Source', 'thememountain-plugin'),esc_html__( 'Determines if the image should be uploaded or pulled from a URL.', 'thememountain-plugin' )),
		ThemeMountain\TM_Vc::get_attach_image_vc_field(),
		ThemeMountain\TM_Vc::get_image_url_vc_field(),
		array(
			'type' => 'textarea_html',
			'heading' => esc_html__( 'Hero Content', 'thememountain-plugin' ),
			'param_name' => 'content',
			'value' => '',
			'admin_label' => true,
			'description' => esc_html__( 'Enter hero content.', 'thememountain-plugin' ),
		),
		ThemeMountain\TM_Vc::get_rich_text_editor_background_color_vc_field(),
		// Please add an option to Split Hero With Split Content Column #1141
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Show Media Column on Mobile', 'thememountain-plugin' ),
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'param_name' => 'show_media_column_on_mobile',
			'std' => '',
			'description' => esc_html__( 'Determines if the media column will be shown on mobile. If deselected, the media column will be hidden from 768px and downwards.', 'thememountain-plugin' ),
		),
		// extra css class name
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra Class Name', 'thememountain-plugin' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style this component differently, then use the extra class name field to add one or several class names and then refer to it in your css file or under Appearance > Customize > Additional CSS.', 'thememountain-plugin' ),
			),
		// Design Options
		array(
			'group' => 'Design Options',
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Text Color', 'thememountain-plugin' ),
			'param_name' => 'text_color',
			'std' => '#FFFFFF',
			'description' => '',
		),
		/** background color with gradient support */
		array(
			'group' => 'Design Options',
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Overlay Background Color', 'thememountain-plugin' ),
			'param_name' => 'split_content_bkg_color',
			'std' => 'rgba(0,0,0,0.5)',
			'description' => '',
		),
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'checkbox',
			'heading' => esc_html__( 'Use gradient', 'thememountain-plugin' ),
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'param_name' => 'background_use_gradient',
			'std' => '',
			'description' => esc_html__( 'This will create a background image gradient, which means that if a background image has been set, the gradient will replace it. If selected, Background Color will be used as the start color for the gradient.', 'thememountain-plugin' ),
		),
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'End Color', 'thememountain-plugin' ),
			'param_name' => 'background_gradient_end_color',
			'std' => '',
			'dependency' => array('element' => 'background_use_gradient','value'=>'true'),
		),
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Angle', 'thememountain-plugin' ),
			'param_name' => 'background_gradient_angle',
			'description'=> esc_html__( 'Determines the angle of the gradient, acceptable values range from 0-360', 'thememountain-plugin' ),
			'dependency' => array('element' => 'background_use_gradient','value'=>'true'),
		),
		/** end background color with gradient support */
		// ANIMATION
		array(
			'group' => 'Animation',
			'type' => 'dropdown',
			'heading' => esc_html__( 'Content Animation Type', 'thememountain-plugin' ),
			'param_name' => 'content_animation',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['animation'],
			'std' => '',
			'description'=> esc_html__( 'Determines the type of animation that will be applied to the element.', 'thememountain-plugin' ),
			'save_always' => true,
		),
		array(
			'group' => 'Animation',
			'type' => 'textfield',
			'heading' => esc_html__( 'Content Animation Duration', 'thememountain-plugin' ),
			'param_name' => 'content_animation_duration',
			'value' => '1000',
			'dependency' => array('element' => 'content_animation','not_empty' => TRUE ),
			'description'=> esc_html__( 'Determines the duration of the animation. Expressed in milliseconds i.e. 1000 represents 1 second.', 'thememountain-plugin' ),
		),
		array(
			'group' => 'Animation',
			'type' => 'textfield',
			'heading' => esc_html__( 'Content Animation Delay', 'thememountain-plugin' ),
			'param_name' => 'content_animation_delay',
			'value' => '0',
			'dependency' => array('element' => 'content_animation','not_empty' => TRUE ),
			'description'=> esc_html__( 'Determines the duration before the animation should begin upon entering the viewport. Expressed in milliseconds i.e. 100 represents 0.1 second.', 'thememountain-plugin' ),
		),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Animation Threshold', 'thememountain-plugin' ),
			'param_name' => 'content_animation_threshold',
			'value' => '0.5',
			'dependency' => array('element' => 'content_animation','not_empty' => TRUE ),
			'description' => esc_html__( 'Determines how much of the element should be in the viewport before the animation begins. Expressed as a decimal from 0.1 to 0.9, where 0.1 represents 10%.', 'thememountain-plugin' ),
		),
	),
	'js_view' => 'TmTabView'
) );

class WPBakeryShortCode_tm_split_hero_with_split_contents_item extends WPBakeryShortCode_tm_tab_item {

}
