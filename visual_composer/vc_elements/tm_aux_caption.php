<?php
/**
 * tm_aux_caption
 * @since 	1.0
 * @see        tm_slider_caption
 */

vc_map( array(
	'name' => esc_html__( 'Caption', 'thememountain-plugin' ),
	'base' => 'tm_aux_caption',
	'icon'      => 'tm_vc_icon_text_block',
	'is_container' => true,
	"as_child" => array('only' => 'tm_slider_item,tm_fullscreen_presentation_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
	'description' => '',
	'params' => array(
		// margins
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Margin Bottom', 'thememountain-plugin' ),
			'param_name' => 'margin_bottom',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['spacing_notches'],
			'std' => '30',
			'description' => esc_html__( 'Determines the bottom margin from 768px and upwards.', 'thememountain-plugin' ),
			),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Margin Bottom on Mobile', 'thememountain-plugin' ),
			'param_name' => 'margin_bottom_mobile',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['spacing_notches'],
			'std' => '30',
			'description' => esc_html__( 'Determines the bottom margin from 768px and downwards.', 'thememountain-plugin' ),
			),
		// content
		array(
			'type' => 'textarea_html',
			'heading' => esc_html__( 'Caption', 'thememountain-plugin' ),
			'param_name' => 'content',
			'value' => '',
			'admin_label' => true,
			'description' => esc_html__( 'Enter the caption here', 'thememountain-plugin' )
		),
		ThemeMountain\TM_Vc::get_rich_text_editor_background_color_vc_field(),
		array(
			'group' => esc_html__( 'Display Options', 'thememountain-plugin' ),
			'type' => 'checkbox',
			'heading' => esc_html__( 'Insert Caption On Same Line', 'thememountain-plugin' ),
			'param_name' => 'display_inline',
			'value' => array( esc_html__( 'Display inline', 'thememountain-plugin' ) => 'true' ),
			'std' => '',
			'description' => esc_html__( 'Determines if the caption should be inserted on its own line or alignd next to the previous caption.', 'thememountain-plugin' )
			),
		// Content Animation
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Content Animation Preset', 'thememountain-plugin' ),
			'param_name' => 'content_animation',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['animation'],
			'std' => 'fadeIn',
			'description'=> esc_html__( 'Determines the type of animation that will be applied to the caption.', 'thememountain-plugin' ),
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Content Animation Duration', 'thememountain-plugin' ),
			'param_name' => 'content_animation_duration',
			'dependency' => array('element' => 'content_animation','not_empty' => TRUE ),
			'value'=>'1000',
			'description'=> esc_html__( 'Determines the duration of the animation. Expressed in milliseconds i.e. 1000 represents 1 second.', 'thememountain-plugin' ),
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Content Animation Delay', 'thememountain-plugin' ),
			'param_name' => 'content_animation_delay',
			'dependency' => array('element' => 'content_animation','not_empty' => TRUE ),
			'value'=>'0',
			'description'=> esc_html__( 'Determines the duration before the animation should begin upon entering the viewport. Expressed in milliseconds i.e. 100 represents 0.1 second.', 'thememountain-plugin' ),
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Animation Threshold', 'thememountain-plugin' ),
			'param_name' => 'content_animation_threshold',
			'value' => '0.5',
			'dependency' => array('element' => 'content_animation','not_empty' => TRUE ),
			'description' => esc_html__( 'Determines how much of the element should be in the viewport before the animation begins. Expressed as a decimal from 0.1 to 0.9, where 0.1 represents 10%.', 'thememountain-plugin' ),
		),
	)
) );

class WPBakeryShortCode_tm_aux_caption extends WPBakeryShortCode {
}
