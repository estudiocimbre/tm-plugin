<?php
vc_map( array(
	'name' => esc_html__( 'Tab', 'thememountain-plugin' ),
	'base' => 'tm_tab_item',
	'allowed_container_element' => 'vc_row',
	'is_container' => true,
	'content_element' => false,
	'description' => '',
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Title', 'thememountain-plugin' ),
			'param_name' => 'title',
			'description' => esc_html__( 'Determines the tab link title.', 'thememountain-plugin' )
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Make this active', 'thememountain-plugin' ),
			'param_name' => 'is_active',
			'value' => array( esc_html__( 'Make this active', 'thememountain-plugin' ) => 'true' ),
			'std' => '',
			'description' => esc_html__( 'Determines if this tab will be active upon page load.', 'thememountain-plugin' )
			),
		array(
			"type" => "textarea_html",
			"holder" => "div",
			"heading" => esc_html__("Message", 'thememountain-plugin'),
			"param_name" => "content",
			"value" => '',
			"description" => esc_html__("Enter tab content.", 'thememountain-plugin')
			),
		ThemeMountain\TM_Vc::get_rich_text_editor_background_color_vc_field(),
		array(
			'type' => 'tab_id',
			'heading' => esc_html__( 'Tab ID', 'thememountain-plugin' ),
			'param_name' => "tab_id"
		)
	),
	'js_view' => 'TmTabView'
) );
