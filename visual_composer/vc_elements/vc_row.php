<?php
namespace ThemeMountain;
/**
* vc_row (custom)
*/

$map_data = array(
	'name' => esc_html__( 'Row / Section Block', 'thememountain-plugin' ),
	'base' => 'vc_row',
	'is_container' => true,
	'icon' => 'tm_vc_icon_row',
	'show_settings_on_create' => false,
	'category' => esc_html__( 'ThemeMountain', 'thememountain-plugin' ),
	'description' => '',
	'params' => array(
		// tm_row add new height option #1072
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Height', 'thememountain-plugin' ),
			'param_name' => 'section_block_height',
			'value' => array (
				esc_html__('Auto', 'thememountain-plugin') => 'auto',
				esc_html__('Window Height', 'thememountain-plugin') => 'window-height',
				esc_html__('Custom', 'thememountain-plugin') => 'custom',
			),
			'std' => 'keep',
			'save_always' => true,
			'description' => esc_html__( 'Determines the height of the section block.', 'thememountain-plugin' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Custom Height', 'thememountain-plugin' ),
			'param_name' => 'section_block_custom_height_value',
			'value' => '500px',
			'description'=> esc_html__( 'Enter a custom height value i.e. 500, which represents 500px.', 'thememountain-plugin' ),
			'dependency' => array('element' => 'section_block_height','value'=>'custom'),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Clear Height on Mobile', 'thememountain-plugin' ),
			'param_name' => 'clear_section_block_height_on_mobile',
			'std' => '',
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'description'=> esc_html__( 'Determines if height should be cleared from 768px and downwards.', 'thememountain-plugin' ),
			'dependency' => array('element' => 'section_block_height','value'=>array('window-height','custom')),
		),
		// Full Width  boolean force_fullwidth If set to true, adds the classes .full-width.collapse to div.section-block.replicable-content.row
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Full Width', 'thememountain-plugin' ),
			'param_name' => 'force_fullwidth',
			'std' => '',
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'description' => esc_html__( 'Forces the row to become full width.', 'thememountain-plugin' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Equalize', 'thememountain-plugin' ),
			'param_name' => 'equalize',
			'std' => '',
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'description' => esc_html__( 'Determines if columns in this row should become the same height i.e. equalized based on the tallest column.', 'thememountain-plugin' ),
			),
		/** This will implement the freeze plugin for the section, which allows users to make any section sticky while scrolling. This is useful when creating project layouts and they would like to stick say social icons or brief info in a section that remain fixed while the other content scrolls.
		 * Ref: I need to you to add a make sticky option to section-block/row like we have for section #1108
		 */
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Make Section Sticky', 'thememountain-plugin' ),
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'param_name' => 'make_section_sticky',
			'std' => '',
			'description' => esc_html__( 'Determines if the section should become sticky upon scroll.', 'thememountain-plugin' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra Space Top', 'thememountain-plugin' ),
			'param_name' => 'extra_space_top',
			'value' => '80',
			'dependency' => array('element' => 'make_section_sticky','value'=>'true'),
			'description' => esc_html__( 'Determines how much extra space should be added to the top of the sticky section. Commonly, you would set this to the height of your header so that the sticky section appears below the header.', 'thememountain-plugin' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra Space Bottom', 'thememountain-plugin' ),
			'param_name' => 'extra_space_bottom',
			'value' => '0',
			'dependency' => array('element' => 'make_section_sticky','value'=>'true'),
			'description' => esc_html__( 'Determines how much extra space should be added to the bottom of the sticky section.', 'thememountain-plugin' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Push Section', 'thememountain-plugin' ),
			'param_name' => 'push_section',
			'value' => '.footer',
			'dependency' => array('element' => 'make_section_sticky','value'=>'true'),
			'description' => esc_html__( 'Determines which section in your layout should force the sticky section to unstick from its position when it reaches the bottom of the section. Accepts IDs and Classes. Default push section is the site footer.', 'thememountain-plugin' ),
		),
		// end #1108
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Columns on Tablet', 'thememountain-plugin' ),
			'param_name' => 'columns_on_tablet',
			'value' => array (
				esc_html__('Keep columns', 'thememountain-plugin') => 'keep',
				esc_html__('Force to two columns', 'thememountain-plugin') => 'force_2',
				esc_html__('Force to single column', 'thememountain-plugin') => 'force_1',
				),
			'std' => 'keep',
			'save_always' => true,
			'description' => esc_html__( 'Determines if columns should be broken down to two or a single column on tablet, from 960px downwards.', 'thememountain-plugin' ),
			),
		/**
		 * Reserved
		 */
    	array(
    		//'group' => esc_html__( 'Reserved', 'thememountain-plugin' ),
    		'type' => 'checkbox',
    		'heading' => esc_html__( 'Use Font Color', 'thememountain-plugin' ),
    		'param_name' => 'set_font_color',
    		'value' => array( esc_html__( 'Determines the color for this row or section block.', 'thememountain-plugin' ) => 'yes' ),
    		'std' => '',
    		'description' => '',
    	),
		array(
			//'group' => esc_html__( 'Reserved', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Font Color', 'thememountain-plugin' ),
			'param_name' => 'font_color',
			'std' => '#666666',
			'dependency' => array('element' => 'set_font_color','value'=>'yes'),
			'description' => '',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'ID', 'thememountain-plugin' ),
			'param_name' => 'el_id',
			'description'=> esc_html__( 'If you wish to scroll to or link to this section, then use the ID field to give the section an unique ID.', 'thememountain-plugin' ),
			),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra Class Name', 'thememountain-plugin' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style this component differently, then use the extra class name field to add one or several class names and then refer to it in your css file or under Appearance > Customize > Additional CSS.', 'thememountain-plugin' ),
			),
		/**
		 * Spacing
		 */
		/* Note: clears all section block padding by adding no-padding to div.section-block */
    	array(
    		'group' => esc_html__( 'Spacing', 'thememountain-plugin' ),
    		'type' => 'checkbox',
    		'heading' => esc_html__( 'Clear All Padding', 'thememountain-plugin' ),
    		'param_name' => 'clear_all_padding',
    		'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'yes' ),
    		'std' => '',
    		'description' => esc_html__( 'Determines if top/bottom padding of the row/section-block should be cleared.', 'thememountain-plugin' ),
    	),
		array(
			'group' => esc_html__( 'Spacing', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Padding Top', 'thememountain-plugin' ),
			'param_name' => 'padding_top',
			'value' => TM_Vc::$vc_elements_variable['spacing_notches'],
			'std' => 'inherit',
			'description' => esc_html__( 'Determines the top padding of the row/section block', 'thememountain-plugin' ),
		),
		array(
			'group' => esc_html__( 'Spacing', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Padding Bottom', 'thememountain-plugin' ),
			'param_name' => 'padding_bottom',
			'value' => TM_Vc::$vc_elements_variable['spacing_notches'],
			'std' => 'inherit',
			'description' => esc_html__( 'Determines the bottom padding of the row/section block', 'thememountain-plugin' ),
		),
		/**
		 * Design Options
		 */
    	array(
    		'group' => esc_html__( 'Background', 'thememountain-plugin' ),
    		'type' => 'checkbox',
    		'heading' => esc_html__( 'Background color or image', 'thememountain-plugin' ),
    		'param_name' => 'use_background',
    		'value' => array( esc_html__( 'Use background color or image.', 'thememountain-plugin' ) => 'yes' ),
    		'std' => '',
    		'description' => esc_html__( 'Determines if the row/section block should receive a background color and/or image.', 'thememountain-plugin' ),
    	),
    	// background color with gradient support
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Background Color', 'thememountain-plugin' ),
			'param_name' => 'background_color',
			'std' => '#FFFFFF',
			'dependency' => array('element' => 'use_background','value'=>'yes'),
			'description' => '',
		),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'checkbox',
			'heading' => esc_html__( 'Use gradient for background', 'thememountain-plugin' ),
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'param_name' => 'background_use_gradient',
			'std' => '',
			'description' => esc_html__( 'This will create a background image gradient, which means that if a background image has been set, the gradient will replace it. If selected, Background Color will be used as the start color for the gradient.', 'thememountain-plugin' ),
			'dependency' => array('element' => 'use_background','value'=> 'yes'),
		),
		// Add gradient animation for section blocks #1066
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'checkbox',
			'heading' => esc_html__( 'Animate Gradient Background', 'thememountain-plugin' ),
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'param_name' => 'background_use_animated_gradient',
			'std' => '',
			'description' => esc_html__( 'Determines if background gradient should be animated.', 'thememountain-plugin' ),
			'dependency' => array('element' => 'background_use_gradient','value'=> 'true'),
		),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Animated Gradient Duration', 'thememountain-plugin' ),
			'param_name' => 'background_animated_gradient_duration',
			'value' => '15000',
			'description' => esc_html__( 'Determines the duration of the animation. Expressed in milliseconds i.e. 15000 represents 1.5 seconds.', 'thememountain-plugin' ),
			'dependency' => array('element' => 'background_use_gradient','value'=> 'true'),
			),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'End Background Color Gradient', 'thememountain-plugin' ),
			'param_name' => 'background_gradient_end_color',
			'std' => '',
			'dependency' => array('element' => 'background_use_gradient','value'=>'true'),
		),
		// #1066
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'End Background Color Gradient 2', 'thememountain-plugin' ),
			'param_name' => 'background_gradient_end_color_2',
			'std' => '',
			'dependency' => array('element' => 'background_use_animated_gradient','value'=>'true'),
		),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'End Background Color Gradient 3', 'thememountain-plugin' ),
			'param_name' => 'background_gradient_end_color_3',
			'std' => '',
			'dependency' => array('element' => 'background_use_animated_gradient','value'=>'true'),
		),
		// end #1066
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Angle for the Background Color Gradient', 'thememountain-plugin' ),
			'param_name' => 'background_gradient_angle',
			'description'=> esc_html__( 'Determines the angle of the gradient, acceptable values range from 0-360', 'thememountain-plugin' ),
			'dependency' => array('element' => 'background_use_gradient','value'=>'true'),
		),
		// end background color with gradient support
		/** overlay **/
    	array(
    		'group' => esc_html__( 'Background', 'thememountain-plugin' ),
    		'type' => 'checkbox',
    		'heading' => esc_html__( 'Add overlay', 'thememountain-plugin' ),
    		'param_name' => 'add_overlay',
    		'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
    		'std' => '',
    		'description' => '',
    		'dependency' => array('element' => 'use_background','value'=> 'yes'),
    	),
    	// Overlay background color with gradient support
		array(
			'group' => 'Background',
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Overlay Background Color', 'thememountain-plugin' ),
			'param_name' => 'overlay_background_color',
			'std' => 'rgba(0,0,0,0.3)',
			'description' => '',
			'dependency' => array('element' => 'add_overlay','value'=>'true'),
			),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'checkbox',
			'heading' => esc_html__( 'Use gradient for overlay background', 'thememountain-plugin' ),
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'param_name' => 'overlay_background_use_gradient',
			'std' => '',
			'description' => esc_html__( 'This will create a background image gradient, which means that if a background image has been set, the gradient will replace it. If selected, Background Color will be used as the start color for the gradient.', 'thememountain-plugin' ),
			'dependency' => array('element' => 'add_overlay','value'=>'true'),
		),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'End Overlay Background Color Gradient', 'thememountain-plugin' ),
			'param_name' => 'overlay_background_gradient_end_color',
			'std' => '',
			'dependency' => array('element' => 'overlay_background_use_gradient','value'=>'true'),
		),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Angle for the Overlay Background Color Gradient', 'thememountain-plugin' ),
			'param_name' => 'overlay_background_gradient_angle',
			'description'=> esc_html__( 'Determines the angle of the gradient, acceptable values range from 0-360', 'thememountain-plugin' ),
			'dependency' => array('element' => 'overlay_background_use_gradient','value'=>'true'),
		),
		/** background image */
		TM_Vc::get_image_selector_vc_field(esc_html__( 'Background', 'thememountain-plugin' ),esc_html__( 'Image Source', 'thememountain-plugin'),esc_html__( 'Determines if the image should be uploaded or pulled from a URL.', 'thememountain-plugin' ),array('element' => 'use_background','value'=>'yes')),
		TM_Vc::get_attach_image_vc_field(esc_html__( 'Background', 'thememountain-plugin' )),
		TM_Vc::get_image_url_vc_field(esc_html__( 'Background', 'thememountain-plugin' )),
		// Row/Section BLock option request for background images #1129
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Background Size', 'thememountain-plugin' ),
			'param_name' => 'section_block_background_size',
			'value' => array (
				esc_html__('Cover', 'thememountain-plugin') => 'cover',
				esc_html__('Contain', 'thememountain-plugin') => 'contain',
			),
			'std' => 'cover',
			'dependency' => array('element' => 'use_background','value'=>'yes'),
		),
		array(
			'group' => esc_html__( 'Background', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Background Repeat', 'thememountain-plugin' ),
			'param_name' => 'section_block_background_repeat',
			'value' => array (
				esc_html__('Repeat', 'thememountain-plugin') => 'repeat',
				esc_html__('No-repeat', 'thememountain-plugin') => 'no-repeat',
			),
			'std' => 'repeat',
			'dependency' => array('element' => 'use_background','value'=>'yes'),
		),
	),
	'js_view' => 'VcRowView'
);

// vc_column
vc_map($map_data);

// vc_column_inner
$map_data['base'] = 'vc_row_inner';
$map_data['name'] = esc_html__( 'Row / Section Block (Inner)', 'thememountain-plugin' );
$map_data['content_element'] = false;
vc_map($map_data);
