<?php
vc_map( array(
	'name' => esc_html__( 'Socialize Social Share Item', 'thememountain-plugin' ),
	'base' => 'tm_socialize_item',
	// 'allowed_container_element' => false,
	'is_container' => true,
	"as_child" => array('only' => 'tm_socialize_holder'),
	"content_element" => FALSE,
	'description' => '',
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Title', 'thememountain-plugin' ),
			'param_name' => 'title',
			'description' => esc_html__( 'Enter a slide title. Used to identify this slide in the Visual Composer.', 'thememountain-plugin' ),
		),


		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Social Network', 'thememountain-plugin' ),
			'param_name' => 'social_network',
			'admin_label' => true,
			'value' => array(
				esc_html__( 'None', 'thememountain-plugin' ) => '',
				esc_html__( 'Email', 'thememountain-plugin' ) => 'email',
				esc_html__( 'Facebook', 'thememountain-plugin' ) => 'facebook',
				esc_html__( 'GooglePlus', 'thememountain-plugin' ) => 'googleplus',
				esc_html__( 'LinkedIn', 'thememountain-plugin' ) => 'linkedin',
				esc_html__( 'Pinterest', 'thememountain-plugin' ) => 'pinterest',
				esc_html__( 'Twitter', 'thememountain-plugin' ) => 'twitter',
				),
			'std' => '',
			'description' => esc_html__( 'Determines the social network to show.', 'thememountain-plugin' ),
			),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Via Username', 'thememountain-plugin' ),
			'param_name' => 'via_username',
			'value' => '',
			'description'=> esc_html__( 'Enter your Twitter username if you want them to mention you in their tweet. Will appear as @username.', 'thememountain-plugin' ),
			'dependency' => array('element' => 'social_network','value'=>'twitter'),
		),
		// 'group' => 'Design Options',
		// background color with gradient support
		array(
			'group' => 'Design Options',
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Icon Color', 'thememountain-plugin' ),
			'param_name' => 'icon_color',
			'std' => '#666666',
		),
		array(
			'group' => 'Design Options',
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Icon Color Hover', 'thememountain-plugin' ),
			'param_name' => 'icon_color_hover',
			'std' => '',
		),
	),
	'js_view' => 'TmTabView'
) );

class WPBakeryShortCode_tm_socialize_item extends WPBakeryShortCode_tm_tab_item {

}
