<?php
vc_map( array(
	'name' => esc_html__( 'Timeline Item', 'thememountain-plugin' ),
	'base' => 'tm_timeline_item',
	'allowed_container_element' => 'tm_timeline_holder',
	'is_container' => true,
	'content_element' => false,
	'description' => '',
	'params' => array(
		/** Please update VC timeline element #963 */
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Is this a new timeline section', 'thememountain-plugin' ),
			'param_name' => 'is_new_timeline_section',
			'value' => array( esc_html__( 'Yes', 'thememountain-plugin' ) => 'true' ),
			'std' => '',
			'description'=> esc_html__( 'Determines if this timeline item represents a new section i.e. creates a break in the timeline.', 'thememountain-plugin' ),
			),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Entry Alignment', 'thememountain-plugin' ),
			'param_name' => 'entry_alignment',
			'value' => array(
				esc_html__( 'Left', 'thememountain-plugin' ) => 'left',
				esc_html__( 'Right', 'thememountain-plugin' ) => 'right',
				),
			'std' => 'left',
			'description' => esc_html__( 'Determines content alignment when the timeline alignment is set to center. This is so that timeline entries can be variated from left/right.', 'thememountain-plugin' )
			),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Entry Title', 'thememountain-plugin' ),
			'param_name' => 'title',
			'description' => esc_html__( 'Enter timeline entry title.', 'thememountain-plugin' )
		),
		array(
			'type' => 'textarea_html',
			'heading' => esc_html__( 'Description', 'thememountain-plugin' ),
			'param_name' => 'content',
			'admin_label' => true,
			'dependency' => array('element' => 'is_hero_content','value'=>'true'),
			'description'=> esc_html__( 'Enter timline entry description.', 'thememountain-plugin' ),
			),
		ThemeMountain\TM_Vc::get_rich_text_editor_background_color_vc_field('',array('element' => 'is_hero_content','value'=>'true')),
		// Spacing
		array(
			'group' => esc_html__( 'Spacing', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Padding Bottom', 'thememountain-plugin' ),
			'param_name' => 'padding_bottom',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['spacing_notches'],
			'std' => '30',
			'description' => esc_html__( 'Determines the bottom padding of the entry i.e. 30 represents 30px.', 'thememountain-plugin' ),
		),
		// Design Options
		// timeline_content_styling (Timeline Content Styling)
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Timeline Content Styling', 'thememountain-plugin' ),
			'param_name' => 'timeline_content_styling',
			'value' => array(
				esc_html__( 'None', 'thememountain-plugin' ) => '',
				esc_html__( 'Boxed', 'thememountain-plugin' ) => 'boxed',
				esc_html__( 'Boxed with rounded corners', 'thememountain-plugin' ) => 'boxed_rounded',
				),
			'std' => '',
			'description' => esc_html__( 'Determines if the timeline content should be wrapped in a box.', 'thememountain-plugin' )
			),
		// box_size (Box Size)
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Box Size', 'thememountain-plugin' ),
			'param_name' => 'box_size',
			'value' =>	array(
				esc_html__('Medium', 'thememountain-plugin') => 'medium',
				esc_html__('Small', 'thememountain-plugin') => 'small',
				esc_html__('Large', 'thememountain-plugin') => 'large',
				esc_html__('Xlarge', 'thememountain-plugin') => 'xlarge',
				esc_html__('Custom', 'thememountain-plugin') => 'custom',
			),
			'std' => '',
			'dependency' => array('element' => 'timeline_content_styling','value'=>array('boxed','boxed_rounded')),
			'description'=> esc_html__( 'Determines the box size in terms of padding.', 'thememountain-plugin' ),
			),
		// box_top_bottom_padding (Box Top/Bottom Padding)
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Box Top/Bottom Padding', 'thememountain-plugin' ),
			'param_name' => 'box_top_bottom_padding',
			'value' => '15',
			'dependency' => array('element' => 'box_size','value'=>'custom'),
			'description' => esc_html__( 'Determines the vertical padding of the box.', 'thememountain-plugin' )
		),
		// box_left_right_padding (Box Left/Right Padding)
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Box Left/Right Padding', 'thememountain-plugin' ),
			'param_name' => 'box_left_right_padding',
			'value' => '15',
			'dependency' => array('element' => 'box_size','value'=>'custom'),
			'description' => esc_html__( 'Determines the horizontal padding of the box.', 'thememountain-plugin' )
		),
		// box_background_color (Box Background Color)
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Box Background Color', 'thememountain-plugin' ),
			'param_name' => 'box_background_color',
			'std' => '',
			'dependency' => array('element' => 'timeline_content_styling','value'=>array('boxed','boxed_rounded')),
			),
		// box_border_color (Box Border Color)
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Box Border Color', 'thememountain-plugin' ),
			'param_name' => 'box_border_color',
			'std' => '',
			'dependency' => array('element' => 'timeline_content_styling','value'=>array('boxed','boxed_rounded')),
			),
		// ANIMATION for date
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Date Animation Type', 'thememountain-plugin' ),
			'param_name' => 'date_animation',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['animation'],
			'std' => '',
			'description'=> esc_html__( 'Determines the type of animation that will be applied to the element.', 'thememountain-plugin' ),
			'save_always' => true,
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Date Animation Duration', 'thememountain-plugin' ),
			'param_name' => 'date_animation_duration',
			'value' => '1000',
			'dependency' => array('element' => 'date_animation','not_empty' => TRUE ),
			'description'=> esc_html__( 'Determines the duration of the animation. Expressed in milliseconds i.e. 1000 represents 1 second.', 'thememountain-plugin' ),
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Date Animation Delay', 'thememountain-plugin' ),
			'param_name' => 'date_animation_delay',
			'value' => '0',
			'dependency' => array('element' => 'date_animation','not_empty' => TRUE ),
			'description'=> esc_html__( 'Determines the duration before the animation should begin upon entering the viewport. Expressed in milliseconds i.e. 100 represents 0.1 second.', 'thememountain-plugin' ),
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Date Animation Threshold', 'thememountain-plugin' ),
			'param_name' => 'date_animation_threshold',
			'value' => '0.5',
			'dependency' => array('element' => 'date_animation','not_empty' => TRUE ),
			'description' => esc_html__( 'Determines how much of the element should be in the viewport before the animation begins. Expressed as a decimal from 0.1 to 0.9, where 0.1 represents 10%.', 'thememountain-plugin' ),
			),
		// ANIMATION for description
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Description Animation Type', 'thememountain-plugin' ),
			'param_name' => 'description_animation',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['animation'],
			'std' => '',
			'description'=> esc_html__( 'Determines the type of animation that will be applied to the element.', 'thememountain-plugin' ),
			'save_always' => true,
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Description Animation Duration', 'thememountain-plugin' ),
			'param_name' => 'description_animation_duration',
			'value' => '1000',
			'dependency' => array('element' => 'description_animation','not_empty' => TRUE ),
			'description'=> esc_html__( 'Determines the duration of the animation. Expressed in milliseconds i.e. 1000 represents 1 second.', 'thememountain-plugin' ),
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Description Animation Delay', 'thememountain-plugin' ),
			'param_name' => 'description_animation_delay',
			'value' => '0',
			'dependency' => array('element' => 'description_animation','not_empty' => TRUE ),
			'description'=> esc_html__( 'Determines the duration before the animation should begin upon entering the viewport. Expressed in milliseconds i.e. 100 represents 0.1 second.', 'thememountain-plugin' ),
			),
		array(
			'group' => esc_html__( 'Animation', 'thememountain-plugin' ),
			'type' => 'textfield',
			'heading' => esc_html__( 'Description Animation Threshold', 'thememountain-plugin' ),
			'param_name' => 'description_animation_threshold',
			'value' => '0.5',
			'dependency' => array('element' => 'description_animation','not_empty' => TRUE ),
			'description' => esc_html__( 'Determines how much of the element should be in the viewport before the animation begins. Expressed as a decimal from 0.1 to 0.9, where 0.1 represents 10%.', 'thememountain-plugin' ),
			),
	),
	'js_view' => 'TmTabView'
) );

class WPBakeryShortCode_tm_timeline_item extends WPBakeryShortCode_tm_tab_item {

}
