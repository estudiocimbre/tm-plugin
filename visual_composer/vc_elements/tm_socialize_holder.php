<?php
use ThemeMountain\TM_Vc as TM_Vc;

$tab_id = 'tm_socialize-' . time() . '-' . rand( 0, 100 );
$tab_id_1 = 'tm_socialize-' . time() . '-1-' . rand( 0, 100 );
// ref http://framework.thememountain.com/servelet/avalanche-slider#

vc_map( array(
	"base"      => "tm_socialize_holder",
	"name"      => esc_html__("Socialize Social Share", 'thememountain-plugin'),
	"icon"      => "tm_vc_icon_social_share",
	'class' => 'tm_element_tab_holder',
	'category' => esc_html__( 'ThemeMountain', 'thememountain-plugin' ),
	'show_settings_on_create' => true,
	'is_container' => true,
	'description' => '',
	'params' => array(
		// image
		ThemeMountain\TM_Vc::get_image_selector_vc_field('',esc_html__( 'Image Source', 'thememountain-plugin'), esc_html__( 'Determines if the image should be uploaded or pulled from a URL.', 'thememountain-plugin' )),
		ThemeMountain\TM_Vc::get_attach_image_vc_field(),
		ThemeMountain\TM_Vc::get_image_url_vc_field(),
		//
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Page Description', 'thememountain-plugin' ),
			'param_name' => 'page_description',
			'description'=> esc_html__( 'Enter a description for the page. This description will appear only for those social networks that allow a description to be shared.', 'thememountain-plugin' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'ID', 'thememountain-plugin' ),
			'param_name' => 'el_id',
			'description' => '',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra Class Name', 'thememountain-plugin' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style this component differently, then use the extra class name field to add one or several class names and then refer to it in your css file or under Appearance > Customize > Additional CSS.', 'thememountain-plugin' ),
		),
		array(
			'type' => 'tab_id',
			'heading' => esc_html__( 'Socialize Social Share ID', 'thememountain-plugin' ),
			'param_name' => "tabs_id",
			'value'=> $tab_id,
			'description' => ''
			),
		// 'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
		array(
			'group' => esc_html__( 'Design Options', 'thememountain-plugin' ),
			'type' => 'dropdown',
			'heading' => esc_html__( 'Icon Size', 'thememountain-plugin' ),
			'param_name' => 'icon_size',
			'value' => ThemeMountain\TM_Vc::$vc_elements_variable['sizes'],
			'std' => 'medium',
			'description' =>  esc_html__( 'Determines the icon size in terms of font size.', 'thememountain-plugin' ),
			),
	),
	'custom_markup' => TM_Vc::tabs_custom_markup('social_share',esc_html__( 'Socialize Social Share', 'thememountain-plugin' )),
	'default_content' => '
	[tm_socialize_item title="' . esc_html__( 'Socialize Social Share Item 1', 'thememountain-plugin' ) . '" tab_id="' . $tab_id_1 . '"][/tm_socialize_item]
	',
	'js_view' => 'TmTabsView'
) );

require_once vc_path_dir( 'SHORTCODES_DIR', 'vc-tabs.php' );

class WPBakeryShortCode_tm_socialize_holder extends WPBakeryShortCode_tm_tab_holder {
	// protected $controls_template_file = 'editors/partials/backend_controls_tab.tpl.php';
	protected $predefined_atts = array(
		'tab_id' => TM_NEW_TITLE,
		'title' => ''
		);

	protected function getFileName() {
		return 'tm_socialize_holder';
	}

	public function getTabTemplate() {
		return '<div class="wpb_template">' . do_shortcode( '[tm_socialize_item title="New Socialize Social Share Item" tab_id=""][/tm_socialize_item]' ) . '</div>';
	}
}
