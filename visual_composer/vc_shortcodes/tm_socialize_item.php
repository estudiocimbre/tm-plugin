<?php
namespace ThemeMountain;

$output = $_data_attributes = $_button_title = $_icon_class = $_href_attribute = $_sns_type = '';

extract( shortcode_atts( array(
	'title' => '',
	'social_network' => '',
	'via_username' => '',
	'icon_color' => '#666666',
	'icon_color_hover' => '',
	// attributes passed over from the parent holder element
	'image_url' => '',
	'page_description' => '',
	'icon_size' => '',
	// reserved
	'el_id' => '',
	'el_class' => '',
), $atts ) );

// css ID
	$_css_id = 'tm-socialize-item-'.TM_Shortcodes::tm_serial_number();

// add spaces for class names
	$el_class = ($el_class!== '') ? ' '.esc_attr($el_class) : '';

// data attributes
	// data-network
	$_data_attributes .= " data-network='{$social_network}'";
	// data-url
	$_http = is_ssl() ? 'https://' : 'http://';
	$_url = $_http . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	$_data_attributes .= " data-url='{$_url}'";
	// data-image
	$_data_attributes .= " data-image='{$image_url}'";
	// data-description
	if(!in_array($social_network,array('facebook','googleplus'))) {
		$_data_attributes .= " data-description='{$page_description}'";
		// data-title
		if($title !== '') {
			$_data_attributes .= ' data-title="'.htmlspecialchars(TM_Shortcodes::tm_wp_kses($title),ENT_QUOTES).'"';
		}
	}
	// title
	if($title !== '') {
		$_data_attributes .= ' title="'.htmlspecialchars(TM_Shortcodes::tm_wp_kses($title),ENT_QUOTES).'"';
	}

// Supported SNS
	switch($social_network){
		case 'email':
			$_icon_class = ' icon-email';
			$_href_attribute = 'mailto:friendsname@domain.com?&amp;subject='.esc_html__('Check out this site','thememountain-plugin').'&amp;body='.$page_description.' - '.$_url;
			// data-email
			$_data_attributes .= " data-email='yourfriend@somedomain.com'";
						// data-subject
			$_data_attributes .= " data-subject='".esc_html__('Check out this site','thememountain-plugin')."'";
			break;
		case 'facebook':
			$_icon_class = ' icon-facebook-with-circle';
			$_href_attribute = '#';
			break;
		case 'googleplus':
			$_icon_class = ' icon-google-with-circle';
			$_href_attribute = '#';
			break;
		case 'linkedin':
			$_icon_class = ' icon-linkedin-with-circle';
			$_href_attribute = '#';
			break;
		case 'pinterest':
			$_icon_class = ' icon-pinterest-with-circle';
			$_href_attribute = '#';
			break;
		case 'twitter':
			$_icon_class = ' icon-twitter-with-circle';
			$_href_attribute = '#';
			// data-user
			if($via_username !==''){
				$_data_attributes .= " data-user='".esc_attr($via_username)."'";
			}
			break;
		default:
			$_icon_class = '';
			break;
	}

	$_sns_type = (!empty($social_network)) ? str_replace('use_','',esc_attr($social_network)) : '';

// css
	if($icon_color !== '' && !empty($_sns_type)) {
		TM_Shortcodes::tm_add_inline_css("li.{$_css_id}  a.{$_sns_type} { color: {$icon_color}; }");
	}
	if($icon_color_hover !== '' && !empty($_sns_type)) {
		TM_Shortcodes::tm_add_inline_css("li.{$_css_id}  a.{$_sns_type}:hover { color: {$icon_color_hover}; }");
	}

// construct output
	$_output = "<li class='{$_css_id}'><a class='socialize {$_sns_type}{$el_class}' href='{$_href_attribute}'{$_data_attributes}'><span class='{$icon_size}{$_icon_class}'></span></a></li>";

/* Output */
	TM_Shortcodes::output_shortcode_content('holder_item', $_output);