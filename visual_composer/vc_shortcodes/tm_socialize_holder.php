<?php
namespace ThemeMountain;

$_output_html = $_image_url = '';

extract( shortcode_atts( array(
	// image
	'image_source' => 'image_id',
	'image_id' => '',
	'image_url' => '',
	'page_description' => '',
	'tabs_id' => '',
	// design options
	'icon_size' => 'medium',
	// element id / class
	'el_id' => '',
	'el_class' => '',
), $atts ) );

// css ID
	$_css_id = 'tm-socialize-holder-'.TM_Shortcodes::tm_serial_number();

// Prepare attributes to pass over
	$_image_url = TM_Shortcodes::get_image_url_by_id($image_source,$image_id,$image_url);
	// Encode by using htmlspecialchars with ENT_QUOTES. You never know what kind of strings are passed from the editor.
	$page_description = htmlspecialchars($page_description,ENT_QUOTES);
	$icon_size = htmlspecialchars($icon_size,ENT_QUOTES);

	// Add attributes to pass over to the child elements.
	$content = str_replace('[tm_socialize_item',"[tm_socialize_item image_url='{$_image_url}' page_description='{$page_description}' icon_size='{$icon_size}'",$content);
	// Expand the child shortcodes
	$_output_html = TM_Shortcodes::tm_do_shortcode($content);
	// remove unnecessary P tag
	$_output_html = str_replace('<p><li', '<li', $_output_html);
	$_output_html = str_replace("</li></p>\n", '</li>', $_output_html);
	// wrap around with ul
	$_output_html = '<ul class="social-list list-horizontal">'.$_output_html.'</ul>';

// const argument
	$_args = array(
		'el_id' => esc_attr($el_id),
		'el_class' => esc_attr($el_class),
		'skip_div_wrap' => TRUE,
		'css_id' => $_css_id,
		'translated_width' => 'width-12',
		);

/* Output */
	TM_Shortcodes::output_shortcode_content('section', $_output_html, '','',$_args);
